// ==============================================================================
// --- Informacion autores ---
// Sensor de Turbidez
// Sonda TURBIDEZ KIE TS-300B: https://innovatorsguru.com/ts-300b-arduino-turbidity-sensor/#iLightbox[6581c955eadc19d925d]/0
// Sensor module
// Microcontrolador: NodeMCU ESP-32S
// Autore:
// Data: 2022
// A turbidez pode ser entendida como a medida do espalhamento de luz produzido pela presença de partículas em suspensão ou coloidais,  sendo expressa como Unidade Nefelométrica de Turbidez  (NTU – Nephelometric Turbidity Unity).
// Fonte: https://2engenheiros.com/2017/12/12/turbidez-da-agua/ & https://www.electrosoftcloud.com/esp32-empezando-a-usar-el-bluetooth-spp/
// Nefelometria: Processo de medida da concentração de uma emulsão por comparação de sua transparência com a de uma preparação padrão.
// Cálculo para o sensor de Turbidez: https://www.dfrobot.com/wiki/index.php/File:Relationship_diagram.jpg
// ==============================================================================
//   Hardware:
//   Sensor de Turbidez   Sonda KIE
//   1                    5V
//   2                    Analógico
//   3                    GND
//  ========================================
//   Arduino              Sensor de Turbidez
//   5V                   V
//   GND                  G
//   A0                   A
// ==============================================================================
// --- Protótipo das Funções ---
//double calc_NTU(double volt);
// Llama a las  funciones
// El tipo de dato numérico en R que se utilizan en el código son de dos tipos:
// 1. INT: Cantidad entera  2 bytes o una palabra (varía según compilador)
// 2. Double: Almacena valores reales en doble precisión. 2 palabras (8 bytes)
// 3. Float: Almacena valores reales en punto flotante.  1 palabra (4 bytes)
// ==============================================================================
// --- Variáveis Globais ---
double NTU = 0;
#define SCOUNT  20           // suma de puntos muestreados
int analogBuffer[SCOUNT];    //almacenar el voltaje de la muestra
int analogBufferIndex = 0;
// analogBuffer: Una matriz para recibir los valores de voltaje leídos desde las entradas analógicas. La matriz está organizada como una matriz lineal de muestras.
float voltage, sensorValue  = 0;


// ==============================================================================
// --- Configurações Iniciais ---

void setup() 
{
   Serial.begin(9600); //poner el mismo valor en el serial

  Serial.println("El dispositivo Bluetooth está listo para medir");
  
} //end setup
//-> comunicación serial para . es una salida para verificar el funcionamiento. Imprime dato para hacer seguimiento
// ==============================================================================
// --- Loop Infinito ---

void loop() 
{
   

static unsigned long timepoint; //Unsigned long variables are extended size variables for number storage and store 32 bits (4 bytes)
   //millis es una función que se usa para medir tiempo. Está instrucción te da el tiempo en milisegundos desde que se encendió la tarjeta Arduino. Una de los principales usos es el determinar el tiempo que ha pasado entre dos o más procesos del programa principal. Por ejemplo, para evitar el debouce en un botón de entrada. Es decir, que mide el tiempo entre el primer pulso de entrada del boton hasta el tiempo X. Es decir hasta que pase un tiempo X después del primer impulso, es que se procede a ejecutar la función del botón.
    if(millis()-timepoint>=500U)     //comprobar si ha transcurrido el plazo
    {         
        timepoint = millis();           //obtener la hora actual
        analogBuffer[analogBufferIndex] = analogRead(A4); //read the analog value and store into the buffer cada 500 milisegundos
         //Serial.println(analogRead(A4));  //imprime el promedio de los 30 valores que lee
         analogBufferIndex++;
      //acá cuenta los enteros, es decir, los números de valores que hay en el buffer
     if(analogBufferIndex == SCOUNT) //si la cant de enteros es igual al scount definido
    {
        analogBufferIndex = 0; 
        sensorValue = getMedianNum(analogBuffer,SCOUNT);   // obtiene un valor estable aplicando como filtro un valor medio
        // Leer más valores analógicos estables por algoritmo de filtrado mediano
        voltage = sensorValue * (5.0 / 1024.0);    //convierte el valor analógico a voltaje y lo guarda en el buffer cada 40ms


         Serial.print(voltage);  //imprime el valor que calculamos
         Serial.println(" v");// separamos con una barra    

         //NTU = calc_NTU(voltage);//variable global (NTU) recibe el retorno de la función “calc_NTU” que aplico al “voltaje"
          NTU = (-552,85*voltage)+(2502,92);//variable global (NTU) recibe el retorno de la función “calc_NTU” que aplico al “voltaje"

   
         Serial.print(NTU);  //imprime el valor que calculamos
         Serial.println(" NTU");// separamos con una barra
        
       
     }
     }
  
  delay(250);//actualice cada  2500 ms
  
} //end loop

// ==============================================================================
// --- Função para conversão de tensão para Turbidez em NTU ---
//
//
/*double calc_NTU(float voltage)
{
  double NTU_val;
  NTU_val = (-517.1088566*voltage)+(2348.45995589823); //NTU_val recibe el rdo
  return NTU_val; //devuelve eso para o meu colho (?) -> a la función principal (calc_NTU). Este es el valor que se imprime arriba
} //end calc_NTU
*/

// ==============================================================================
// --- Promedio de valores ---
//
int getMedianNum(int bArray[], int iFilterLen)
{
      int bTab[iFilterLen];
      for (byte i = 0; i<iFilterLen; i++)
      {
      bTab[i] = bArray[i];
      }
      int i, j, bTemp;
      for (j = 0; j < iFilterLen - 1; j++)
      {
      for (i = 0; i < iFilterLen - j - 1; i++)
          {
        if (bTab[i] > bTab[i + 1])
            {
        bTemp = bTab[i];
            bTab[i] = bTab[i + 1];
        bTab[i + 1] = bTemp;
         }
      }
      }
      if ((iFilterLen & 1) > 0)
    bTemp = bTab[(iFilterLen - 1) / 2];
      else
    bTemp = (bTab[iFilterLen / 2] + bTab[iFilterLen / 2 - 1]) / 2;
      return bTemp;
}
