# TURBIDímetro



## Imágenes

![foto](Imágenes/20220705_100621.jpg) 

![foto](Imágenes/20220706_162225.jpg) 

![foto](Imágenes/20220826_145540.jpg) 


## ¿Qué es la turbidez?

La turbidez es el efecto óptico conocido como efecto Tyndall, que se origina al dispersarse los rayos de luz que atraviesan una muestra de agua a causa de los coloides que el líquido puede contener (Galvin, 1999). 

Se mide principalmente en unidades nefelométricas de turbidez (NTU) o unidades de formacina nefelométrica (FNU), según el método utilizado para la medición.

## ¿Cómo se mide la turbidez?
La medición de la turbidez se realiza utilizando un principio óptico. El sensor se compone básicamente de un emisor (fuente) y un detector de luz. Dentro del sensor la fuente emite el haz de luz de cierta longitud de onda que al atravesar la solución, en la que se desee hacer la medición, choca con las partículas presentes generando la dispersión del haz (efecto Tyndall). La cantidad de luz dispersada es directamente proporcional a la turbidez, es decir, mayor luz dispersada mayor turbidez.

![foto](Imágenes/20220826_150251.jpg) 
***

## Materiales para la construcción
Para construir el turbidímetro basado en Arduino se requiere los siguientes equipos:

    1. Arduino Nano
    2. Placa bluetooth HC-05
    3. Sensor de turbidez (en este caso se utilizó marca KIE)
    4. Power Bank 5 a 12 V1
    5. Teléfono celular o equipo con Android

## Funcionamiento
Dentro de la sonda se encuentra el sensor de turbidez que consta de dos leds infrarrojos ubicados a 180° (Imagen XX). Un led funciona como emisor del haz de luz y el otro como un fototransistor que detecta la luz transmitifa. Cuando la luz pasa por una cierta cantidad de agua, la cantidad de luz transmitida depende del grado de turbidez. Cuanto más turbidez tenga el agua, menos luz se transmite. El extremo receptor de la luz convierte la intensidad de la luz transmitida en una magnitud de corriente correspondiente. Si la luz transmitida es grande la corriente es grande y si la luz transmitida es pequeña la corriente es pequeña.

El módulo del sensor de turbidez convierte la salida de señal de corriente del sensor en una señal de voltaje. El módulo tiene una interfaz de salida analógica y digital. La cantidad analógica puede ser probada por el convertidor a/d del microcontrolador para conocer la medición. Luego la señal que llega al arduino, es traducida a unidades NTU mediante la ecuación obtenida de la caibración (ver “Caracterización del equipo”) que vincula el voltaje y la turbidez (NTU). Mediante el display (teléfono celular) es posible divisarlo.

## Armado
La conexión de las placas Node-MCU, Bluetooth HC-05, Turbidity Sensor Kie se muestra en la figura a continuación (falta actualizar imagen con arduino nano):

![foto](Imágenes/conexiones.jpg)

Se recomienda disponer las placas Arduino Nano, Bluetooth HC-05 y el módulo del Turbidity Sensor con sus respectivos cables de conexión dentro de una caja aislada, que cuente con una perforación de entrada para la conexión del Power Bank y una perforación de salida para el detector de turbidez, con el fin de garantizar que las mismas no entren en contacto con agua, químicos o suciedad que pudieran dañarlas. 

## Puesta en marcha
Para poner en funcionamiento el equipo se deberán seguir los siguientes pasos:

    1. Conectar el Power Bank a la placa Arduino Nano.
    2. Conectar mediante bluetooth el equipo con la aplicación TURBIDímetro en el dispositivo Android. Los datos de turbidez deberán aparecer en la pantalla luego de unos segundos. 
    3. Calibrar el equipo.
    4. Sumergir la sonda de turbidez en la muestra a analizar. 


## Caracterización del equipo
- El límite de cuantificación (LOQ) del equipo es de: 58,37 NTU.

Los valores de turbidez son calculados a partir del voltaje registrado por la sonda según la siguiente ecuación:
Turbidez (NTU) = -552,85 * Voltage + 2502,92

Se realizó una verificación de la curva de calibración obtenida con un equipo de referencia (Sonda HORIBA):

![foto](Imágenes/image__1_.png) 

En el caso que el equipo estuviera calibrado la verificación sería:

![foto](Imágenes/image.png)  graficos ntu vs ntu

La caracterización del equipo se realizó utilizando un aceite de corte comercial como patrón, preparando emulsiones de distintas concentraciones del aceite según bibliografía (Lambrou, 2010):

![foto](Imágenes/WhatsApp_Image_2022-07-05_at_3.17.17_PM.jpeg)

Se prepararon 8 soluciones que abarcan un rango de turbidez de 0 a 700 NTU.

## Estado del Proyecto
Actualmente estamos incorporando al código subido la calibración para mejorar la medición del equipo.


## Autores
- Lara Jatar: integrante de CoSensores. Realizó su aporte en el marco de la beca doctoral CONICET que se encuentra en curso en el instituto IIIA-UNSAM.
- Ignacio Borón: Co-Director.
- CoSensores
